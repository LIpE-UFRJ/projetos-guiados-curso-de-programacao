import tkinter as tk
from tkinter import messagebox

class spaceInvaders():
    
    def __init__(self,master,funcaoMover):

        self.mover = funcaoMover

        self.master = master

        self.canvas = tk.Canvas(master, width = 512, height = 512)
        self.canvas.pack()

        self.user = self.canvas.create_polygon(0,512,50,512,25,472,fill='blue')

        self.tiros = []
        self.inimigos = [] 
        
        self.canvas.bind('m', self.direita)
        self.canvas.bind('n', self.esquerda)
        self.canvas.bind('x', self.direita)
        self.canvas.bind('z', self.esquerda)

        self.canvas.bind('a', self.criarTiro)
        self.canvas.bind('<Button-1>', self.criarTiro)
        
        self.canvas.focus_set()

        self.criarInimigos()
        
        self.jogo()


    def direita(self,evento):
        passo = self.mover('direita')
        self.canvas.move(self.user, passo, 0)
    
    def esquerda(self,evento):
        passo = self.mover('esquerda')
        self.canvas.move(self.user, passo, 0)

    def criarTiro(self,evento):
        coordenadas = self.canvas.coords(self.user)
        coordsCentroX = coordenadas[4]
        coordsCentroY = coordenadas[5]
        coordsTiro = [coordsCentroX-3,coordsCentroY-6,\
                      coordsCentroX+3,coordsCentroY]
        tiro = self.canvas.create_rectangle(*coordsTiro,fill='green')
        self.tiros.append(tiro)

    def criarInimigos(self):
        for posX in [100,162,224,286,348]:
            for posY in [0,62]:#[0,62, 122]:
                i = self.canvas.create_rectangle(posX, posY, posX+52, posY+52,fill='red')
                self.inimigos.append(i)

    def jogo(self):

        coordsTiros = []
        coordsInimigos = []

        for tiro in self.tiros:
            self.canvas.move(tiro,0,-1)
            coordsTiros.append(self.canvas.coords(tiro))

        for inimigo in self.inimigos:
            self.canvas.move(inimigo,0,1)
            coordsInimigos.append(self.canvas.coords(inimigo))

        for xi1,yi1,xi2,yi2 in coordsInimigos:
            if yi2 == 512:
                print ('Voce perdeu :(')
                messagebox.showinfo('Tente novamente', 'Voce perdeu :(')
                self.master.destroy()
                return

        indiceTiro = 0
        for x1,y1,x2,y2 in coordsTiros:
            indiceInimigo = 0
            for xi1,yi1,xi2,yi2 in coordsInimigos:
                if (xi1<=x1<=xi2 or xi1<=x2<=xi2) and yi1<=y1<=yi2:
                    self.canvas.delete(self.inimigos.pop(indiceInimigo))
                    self.canvas.delete(self.tiros.pop(indiceTiro))
                indiceInimigo = indiceInimigo + 1
            indiceTiro = indiceTiro + 1

        if self.inimigos == []:
            print ('Voce ganhou! :) ')
            messagebox.showinfo('Parabens!', 'Voce ganhou! :)')
            self.master.destroy()
            return
            
        self.canvas.after(30,self.jogo)

def jogar(funcaoMover):
    root = tk.Tk()

    jogo = spaceInvaders(root,funcaoMover)

    root.mainloop()
