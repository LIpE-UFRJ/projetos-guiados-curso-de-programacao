
import jogos_python as jp
import random

#definiçoes dos objetos
jp.abrir_tela(360, 640, fps= 60)


valor1 = random.randrange(1,9)
valor2 = random.randrange(1,9)
texto = jp.Texto(mensagem= ("Quanto é " + str(valor1) + "+" + str(valor2) + "?"), cor= jp.verde)
galinha = jp.Objeto("galinha.png")
fazenda = jp.Objeto("fazenda.png")
pontuacao = jp.Texto( 0, x= 300)
resultado = jp.Texto("Vitória", x=30, y=300, cor= jp.vermelho)
pintinhos = []

for i in range(3):
    pintinho = jp.Objeto("pintinho.png")
    pintinhos.append( pintinho)

#definição dos estados iniciais
def resetar():
    resultado.visivel = False
    galinha.tecla_mover_para_cima = jp.tecla_w
    galinha.tecla_mover_para_baixo = jp.tecla_s
    galinha.tecla_mover_para_esquerda = jp.tecla_a
    galinha.tecla_mover_para_direita = jp.tecla_d

    galinha.posicao = [150,100]
    galinha.velocidade = 300

    fazenda.posicao = [150,450]

    pontuacao.mensagem = 0

    global valor1, valor2

    valor1 = random.randrange(1,9)
    valor2 = random.randrange(1,9)
    texto.mensagem = ("Quanto é " + str(valor1) + "+" + str(valor2) + "?")



    for i in range(3):
        pintinhos[i].posicao = [i*160, 600]
        pintinhos[i].direcao = [1,1]
        pintinhos[i].solido = False
        pintinhos[i].velocidade = 90


resetar()

#interações e colisões
def movimentacao( objeto ):
    if( objeto.posicao[0] < 0 or objeto.direita > jp.tela.largura ):
        objeto.direcao[0] *= -1
        
    if( objeto.posicao[1] < 30 or objeto.base > jp.tela.altura ):
        objeto.direcao[1] *= -1

def travar_em_cima( objeto):
    if( objeto.y >  460 ):
        galinha.y -= 1


def colisao():
    for i in range(3):
        if( galinha.colidiu_com(pintinhos[i]) ):
            pintinhos[i].posicao = [i*160, 610]
            pontuacao.mensagem += 1 
        if( galinha.colidiu_com(fazenda) ):
            if(pontuacao.mensagem == valor1+valor2):
                resultado.mensagem = "Vitória"
            else:
                resultado.mensagem = "Tente Novamente"
            resultado.visivel = True
            jp.pausar(0.5)
            resetar()
            
for pintinho in pintinhos:
    pintinho.adicionar_movimento( movimentacao )

galinha.adicionar_movimento( travar_em_cima )

jp.adicionar_acao( colisao )

