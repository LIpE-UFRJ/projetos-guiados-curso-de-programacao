import pygame

pygame.font.init()

class Texto():
    def __init__(self, mensagem, cor, x, y):
        self.mensagem = mensagem
        self.x = x
        self.y = y
        self.cor = cor
        self.tipo_de_fonte = None
        self.tamanho_da_fonte = 55
        self.visivel = True

    @property
    def posicao(self):
        return [self.x,self.y]
    @posicao.setter
    def posicao(self, posicao):
        self.x = posicao[0]
        self.y = posicao[1]
    
    @property
    def fonte(self):
        return pygame.font.Font(self.tipo_de_fonte,
                                self.tamanho_da_fonte)

    @property
    def texto(self):
        return self.fonte.render( str(self.mensagem), True, self.cor)
