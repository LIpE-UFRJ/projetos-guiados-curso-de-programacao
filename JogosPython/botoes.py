import pygame

class Botao():
    def __init__(self, x, y, largura, altura, cor):
        self.rect = pygame.Rect( [x, y, largura, altura] )
        self.cor = cor
        self.visivel = True


    @property
    def pressionado(self):
        if( self.rect.collidepoint(pygame.mouse.get_pos()) and
            pygame.mouse.get_pressed()[0]):
            return True
        else:
            return False

    
    
