import jogos_python as jp

jp.abrir_tela(360, 640, jp.verde_escuro)

galinha = jp.Objeto("galinha.png")

galinha.tecla_mover_para_cima = jp.tecla_w
galinha.tecla_mover_para_baixo = jp.tecla_s
galinha.tecla_mover_para_esquerda = jp.tecla_a
galinha.tecla_mover_para_direita = jp.tecla_d

galinha.posicao = [150,100]
galinha.velocidade = 400

ovo = jp.Objeto("ovo.png")

ovo.posicao = [250, 500]
ovo.direcao = [1,1]
ovo.velocidade = 100

def colisao():
    if( galinha.colidiu_com(ovo) ):
        ovo.posicao = [100,300]

def movimentacao( objeto ):
    if( objeto.posicao[0] < 10 or objeto.posicao[0] > 340):
        objeto.direcao[0] = -objeto.direcao[0]
        
    if( objeto.posicao[1] < 10 or objeto.posicao[1] > 620):
        objeto.direcao[1] = -objeto.direcao[1]

jp.adicionar_acao( colisao )
ovo.adicionar_movimento( movimentacao )

