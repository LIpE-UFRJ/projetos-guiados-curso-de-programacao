import jogos_python as jp 

jp.abrir_tela()

fazenda = jp.Objeto("fazenda.png")
largura_da_fazenda = fazenda.largura
fazenda.apagar()
chao = []

for i in range(0, jp.tela.largura, largura_da_fazenda ):
    fazenda = jp.Objeto("fazenda.png")
    fazenda.manter_na_tela = False
    fazenda.estatico = True
    fazenda.y = 500
    fazenda.esquerda = i
    chao.append( fazenda )

chao[5].apagar()
chao[6].apagar()
chao[3].manter_na_tela = True

galinha = jp.Objeto("galinha.png")

galinha.base = chao[1].topo
galinha.direcao = jp.para_baixo
galinha.tecla_mover_para_direita = jp.tecla_d
galinha.tecla_mover_para_esquerda = jp.tecla_a
galinha.virada_para = 'direita'

galinha.tecla_pular = jp.tecla_espaco

def pular( objeto ):
    if( jp.esta_apertada( objeto.tecla_pular)):
        for i in chao:
            if( objeto.tocou_em_cima(i) ):
                objeto.direcao = jp.para_cima
                jp.esperar( 1 )
                objeto.direcao = jp.para_baixo

def virar( objeto ):
    if( objeto.virada_para == 'direita' and\
        jp.esta_apertada( objeto.tecla_mover_para_esquerda ) ):
        objeto.virada_para = 'esquerda'
        objeto.espelhar('horizontalmente')
    elif( objeto.virada_para == 'esquerda' and\
        jp.esta_apertada( objeto.tecla_mover_para_direita ) ):
        objeto.virada_para = 'direita'
        objeto.espelhar('horizontalmente')

galinha.adicionar_movimento( pular )
galinha.adicionar_movimento( virar )
