from tkinter import *
from tkinter import messagebox

tiros = []
inimigos = [] 

def spaceInvaders(master,funcaoMover):
    
    global canvas
    global nave

    canvas = Canvas(master, width = 512, height = 512)
    canvas.pack()    

    nave = canvas.create_polygon(0,512,50,512,25,472,fill='blue')

    canvas.bind('m', lambda evento: direita(evento,funcaoMover))
    canvas.bind('n', lambda evento: esquerda(evento,funcaoMover))
    canvas.bind('x', direita)
    canvas.bind('z', esquerda)

    canvas.bind('a', criarTiro)
    canvas.bind('<Button-1>', criarTiro)
        
    canvas.focus_set()

    criarInimigos()
        
    jogo()

    
def direita(evento,funcaoMover):
    passo = funcaoMover('direita')
    canvas.move(nave, passo, 0)
    
def esquerda(evento,funcaoMover):
    passo = funcaoMover('esquerda')
    canvas.move(nave, passo, 0)

def criarTiro(evento):
    global tiros
    global canvas
    coordenadas = canvas.coords(nave)
    coordsCentroX = coordenadas[4]
    coordsCentroY = coordenadas[5]
    coordsTiro = [coordsCentroX-3,coordsCentroY-6,\
                      coordsCentroX+3,coordsCentroY]
    tiro = canvas.create_rectangle(*coordsTiro,fill='green')
    tiros.append(tiro)

def criarInimigos():
    global canvas
    for posX in [100,162,224,286,348]:
            for posY in [0,62]:
                i = canvas.create_rectangle(posX, posY, posX+52, posY+52,fill='red')
                inimigos.append(i)

def jogo():
    global canvas
    coordsTiros = []
    coordsInimigos = []

    for tiro in tiros:
        canvas.move(tiro,0,-1)
        coordsTiros.append(canvas.coords(tiro))

    for inimigo in inimigos:
        canvas.move(inimigo,0,1)
        coordsInimigos.append(canvas.coords(inimigo))

    for xi1,yi1,xi2,yi2 in coordsInimigos:
        if yi2 == 512:
            print('Voce perdeu :(')
            messagebox.showinfo('Tente novamente', 'Voce perdeu :(')
            root.destroy()
            return

    indiceTiro = 0
    for x1,y1,x2,y2 in coordsTiros:
        indiceInimigo = 0
        for xi1,yi1,xi2,yi2 in coordsInimigos:
            if (xi1<=x1<=xi2 or xi1<=x2<=xi2) and yi1<=y1<=yi2:
                canvas.delete(inimigos.pop(indiceInimigo))
                canvas.delete(tiros.pop(indiceTiro))
            indiceInimigo = indiceInimigo + 1
        indiceTiro = indiceTiro + 1

    if inimigos == []:
        print ('Voce ganhou! :) ')
        messagebox.showinfo('Parabens!', 'Voce ganhou! :)')
        root.destroy()
        return

    canvas.after(30,jogo)

def jogar(funcaoMover):
    global root

    root = Tk()
    spaceInvaders(root,funcaoMover)

    root.mainloop()

